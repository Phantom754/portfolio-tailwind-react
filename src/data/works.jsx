export default [
	{
		title: 'TechHub-Blog',
		imgUrl:
			'https://raw.githubusercontent.com/ShaifArfan/techHub-blog/main/banner.png',
		tech: ['React JS', 'Gatsby JS', 'Sanity.io'],
		workUrl: 'https://github.com/ShaifArfan/techHub-blog',
	},
	{
		title: "Didi Cuisine",
		imgUrl:
			'https://raw.githubusercontent.com/ShaifArfan/shaif-s-cuisine/main/readmeImg/banner.png',
		tech: ['HTML', 'CSS', 'Netlify'],
		workUrl: 'https://github.com/ShaifArfan/shaif-s-cuisine',
	},
	{
		title: "Portfolio Website",
		imgUrl:
			'https://scontent.ftnr5-1.fna.fbcdn.net/v/t1.15752-9/307443324_1102622267061397_213821512050960357_n.png?_nc_cat=109&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeERH49SkbmWNWD10ekXvJdff8ai-YOyLtd_xqL5g7Iu1xwLZ9urPDnhn_D-WJoXD5SdsOnY9LxGBgml7SAxiPb8&_nc_ohc=dhB5WYT-5_4AX8xkUre&tn=KZwtoXqNT3EC8C-L&_nc_ht=scontent.ftnr5-1.fna&oh=03_AVLxkn32B9SA58yXceK2hQOgHIf4KKNRV-0IgRJJ90LvqA&oe=634D27FA',
		tech: ['React JS', 'CSS'],
		workUrl: 'https://github.com/ShaifArfan/AYANs-portfolio',
	},
	{
		title: 'Artistic',
		imgUrl:
			'https://raw.githubusercontent.com/ShaifArfan/artistic/main/readmeImg/banner.png',
		tech: ['React JS', 'CSS'],
		workUrl: 'https://github.com/ShaifArfan/artistic',
	},
];