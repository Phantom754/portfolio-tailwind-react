import { MdWeb, MdDesignServices, MdDeveloperBoard } from "react-icons/md";
import { VscCode } from "react-icons/vsc";
import { FaHandHoldingHeart } from "react-icons/fa";
export default [
    {
        title: "web Design",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore dicta ipsam voluptatibus",
        icon: <MdWeb className="w-full h-full" />
    },
    {
        title: "UI Design",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore dicta ipsam voluptatibus",
        icon: <FaHandHoldingHeart className="w-full h-full" />
    },
    {
        title: "web Development",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore dicta ipsam voluptatibus",
        icon: <VscCode className="w-full h-full" />
    },
]