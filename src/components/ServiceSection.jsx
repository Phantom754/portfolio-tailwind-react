import SectionTitle from "./SectionTitle";
import services from "../data/services";
import ServiceItem from "./ServiceItem";

const ServiceSection = () => {
  return (
    <div className="py-12">
        <SectionTitle id={'service section'}>Our Services</SectionTitle>
        <div className="card-service">
            {services.map((service,index)=>(
                <ServiceItem {...service} key={index}/>
            ))}
        </div>
    </div>
  )
}

export default ServiceSection