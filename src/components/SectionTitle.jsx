import React from 'react'

const SectionTitle = ({children,id}) => {
  return (
    <h1 id={id && id} className="title-section ">
        {children}
    </h1>
  )
}

export default SectionTitle