import React from 'react'
import SectionTitle from './SectionTitle'
import WorkItem from './WorkItem'
import works from '../data/works'

const WorkSection = () => {
  return (
    <div id="work" className='py-12'>
        <SectionTitle id={'work section'}>Recent Work</SectionTitle>
        <div className='grid grid-cols-1 md:grid-cols-2 gap-5'>
          {
            works.map((work,index) => <WorkItem {...work} key={index} />)
          }
        </div>
    </div>
  )
}

export default WorkSection