import React from 'react'

const Footer = () => {
  return (
    <div className='py-2 mb-5 bg-slate-800 text-center text-gray-300 rounded-t-sm'>
        <a href="#hero" className='block text-xl md:text-2xl font-semibold'>Onjaniaina ML</a>
        <a 
            href="mailto:onjaniaina754@gmail.com" 
            className='font-regular text-sm md:text-base hover:text-indigo-400'
        >onjaniaina754@gmail.com</a>
        <p className='text-xs font-regular mt-2 text-gray-500'>
            &copy; Predator-Code Creative  {new Date().getFullYear()} . All rights reserved
        </p>
    </div>
  )
}

export default Footer