import React from 'react'
import SectionTitle from './SectionTitle'

const AboutSection = () => {
  return (
    <div className='flex flex-col md:flex-row items-center justify-start gap-10 md:gap-48 py-20'>
       <div className='w-full md:w-6/12'>
         <SectionTitle>About Me</SectionTitle>
         <p className='text-base text-gray-600 dark:text-gray-300'>
          Lorem ipsum dolor sit amet consectetur adipisicing elit.
          Consectetur praesentium voluptatibus adipisci natus iure ab vel 
          est consequatur placeat, labore velit eveniet, fugiat culpa molestias aperiam quod ullam excepturi nostrum?
         </p>
         <a 
            href="mailto:onjaniaina754@gmail.com" 
            className='block mt-3 text-base md:text-lg font-regular text-gray-700 dark:text-gray-300 underline
            hover:text-indigo-500 dark:hover:text-indigo-500 '
          >
            onjaniaina754@gmail.com</a>
       </div>
       <img 
          src="https://scontent.ftnr5-1.fna.fbcdn.net/v/t1.6435-9/160545007_2915975462017088_6850367945617130214_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=174925&_nc_ohc=E1HNsQbMYLsAX__0bpa&_nc_ht=scontent.ftnr5-1.fna&oh=00_AT-10uuLnsGLoH3hOxjVg_nrtQEBMnZRCHSV7-d6CUn0hA&oe=6351D37D"
          alt="onjaniaina" 
          className='w-full h-1/4 md:w-1/4 rounded-lg object-cover' />
    </div>
  )
}

export default AboutSection