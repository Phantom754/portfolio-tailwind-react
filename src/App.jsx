import { useEffect, useState } from "react"
import AboutSection from "./components/AboutSection";
import Footer from "./components/Footer";
import HeroSection from "./components/HeroSection"
import ServiceSection from "./components/ServiceSection";
import WorkSection from "./components/WorkSection";

const App = () => {
  const [theme, setTheme] = useState(null); 
  useEffect(() => {
    if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
      setTheme('dark');
    }
    else{
      setTheme('ligth')
    }
  },[]);

  useEffect(() => {
    if (theme === 'dark') {
      document.documentElement.classList.add('dark')
    }
    else {
      document.documentElement.classList.remove('dark')
    }
  }, [theme]);

  const handleSwitchTheme = () => {
    setTheme(theme === "dark" ? "ligth" : "dark")
  }
  return (
    <>
      <button type="button" onClick={handleSwitchTheme} className="fixed z-10 right-2 top-2 bg-indigo-500
      text-lg p-1 rounded-md">
        {theme === "dark" ? "🌙":"☀️"}
      </button>
      <div className="font-inter bg-white dark:bg-slate-900">
          <div className="max-w-5xl mx-auto w-11/12">
            <HeroSection />
            <ServiceSection/>
            <WorkSection />
            <AboutSection />
            <Footer />
          </div>
      </div>
    </>
  )
}

export default App